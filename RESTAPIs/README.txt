these steps are performed in a J2EE flavor of eclipse


define:
in eclipse
1. create a Tomcat v8.5+ server
 
build: 
in eclipse
1. right click on the project and select run as -> maven install

deploy: 
in eclipse
1. in the servers tab (window -> show view -> servers), right click on the Tomcat server and select "add and remove"
2. add the RESTAPIs project to the server
1. start (or restart) the server in eclipse

use:
before you can use these APIs, you need to start the Services server (see the README.txt file there)
in your favorite browser
1. browse to http://localhost:8080/restapi/vehicles/create to create a new vehicle
   take note of the sub_??? id. you'll use this in the next setp.
2. in a new tab, browse to http://localhost:8080/restapi/vehicles/setDirection?vehicleSubId=sub_[???]&newDirection=[north|south|east|west] to direct the vehicle
   the vehicle will begin to move in the indicated direction and publish it's status every 5 seconds

there are 2 ways to see the vehicle's status:
1. monitor the Services console from which you launched the Services server, or
2. use the UI feature (see the README.txt file there) 