package tba.restapi;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

/**
 * MessageSender
 * 
 * <p>
 * send a point-to-point message to the vehicle
 * </p>
 *
 */
public class MessageSender {

  private static String messageBrokerUrl = ActiveMQConnection.DEFAULT_BROKER_URL;
  
  // the channel at which the vehicle is listening/subscribed
  private String subChannelId;
  // the message to be sent to the vehicle
  private String payload;
  
  /**
   * @param subChannelId  the channel at which the vehicle is listening/subscribed
   * @param payload  the message to be sent to the vehicle
   */
  public MessageSender(String subChannelId, String payload) {
    this.subChannelId = subChannelId;
    this.payload = payload;
  }
  
  /**
   * send: fire-and-forget
   * @throws JMSException
   */
  public void send() throws JMSException {
    // get a JMS connection and start it
    ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(messageBrokerUrl);
    Connection connection = connectionFactory.createConnection();
    connection.start();
     
    //create a non-transactional session
    Session session = connection.createSession(false,Session.AUTO_ACKNOWLEDGE); 
     
    //The queue subChannelId should already be created by the vehicle
    Destination destination = session.createQueue(subChannelId);
     
    // MessageProducer is used for sending messages to the queue.
    MessageProducer producer = session.createProducer(destination);
     
    // package the payload
    TextMessage message = session.createTextMessage(payload);
     
    // Send the message!
    producer.send(message);

    // release resources and we're done
    connection.close();
  }
   
}
