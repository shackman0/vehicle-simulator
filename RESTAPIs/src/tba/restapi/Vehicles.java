package tba.restapi;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.jms.JMSException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
 
/**
 * Vehicles
 * <p>
 * API for creating and communicating with vehicles.
 * the vehicle service must be listening at localhost:8090
 * </p>
 *
 */
@Path("/vehicles")
public class Vehicles {
 
	/**
	 * this method implements the create vehicle api
	 * 
	 * @return
	 * @throws IOException 
	 */
	@Path("/create")
	@GET
	@Produces("application/json")
	public Response create() throws IOException {
	  // call the create-vehicle service
	  URL url = new URL("http://localhost:8090/create-vehicle");
	  HttpURLConnection con = (HttpURLConnection) url.openConnection();
	  con.setRequestMethod("GET");
	  int status = con.getResponseCode();
	  if (status != 200) {
	    return Response.status(status).build();
	  }
	  // get the response. it contains the pub and sub ids we need to return to the requester.
	  BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
    String inputLine;
    StringBuilder content = new StringBuilder();
    while ((inputLine = in.readLine()) != null) {
      content.append(inputLine);
    }
    in.close();
    // close the http connection
    con.disconnect();
	  // return the result
    String json = content.toString();
	  return Response.ok(json, MediaType.APPLICATION_JSON).build();
	}

	/**
	 * implements the setDirection API
	 * 
	 * @param vehicleSubId
	 * @param newDirection
	 * @return
	 */
	@Path("/setDirection")
	@GET
	@Consumes("text/plain")
	@Produces("application/json")
	public Response setDirection(@QueryParam("vehicleSubId") String vehicleSubId, @QueryParam("newDirection") String newDirection) {
	  /*
	   * validate the parameters
	   * note: normally i would use Swagger to handle this for me. but i don't have swagger installed on my home 'puter yet.
	   */
	  if (StringUtils.isBlank(vehicleSubId)) {
	    return Response.status(Response.Status.BAD_REQUEST).entity("vehicleSubId is missing or empty").build();
	  }
    if (StringUtils.isBlank(newDirection)) {
      return Response.status(Response.Status.BAD_REQUEST).entity("newDirection is missing or empty").build();
    }
    if (!(newDirection.equalsIgnoreCase("north") || newDirection.equalsIgnoreCase("south") || newDirection.equalsIgnoreCase("east") || newDirection.equalsIgnoreCase("west"))) {
      return Response.status(Response.Status.BAD_REQUEST).entity("newDirection is not a valid value. it must be: 'north', 'south', 'east', or 'west'").build();
    }
    try {
      // publish message to channel vehicleSubId
      new MessageSender(vehicleSubId, newDirection).send();
      return  Response.ok("direction set to " + newDirection).build();
    } catch (JMSException e) {
      StringWriter stringWriter = new StringWriter();
      e.printStackTrace(new PrintWriter(stringWriter));
      return  Response.status(Status.INTERNAL_SERVER_ERROR).entity(stringWriter).build();
    }
	}
	
}