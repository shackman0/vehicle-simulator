this software belongs to Floyd Shackelford. 

you are allowed to scrutinize this software for the purposes of evaluating my skills as a software engineer. 
any other use requires advance permission.

This project involves a "vehicle" that travels in real time either North, South, East, or West. 
Use the UI to track it's progress as well as give it directional commands.

It demonstrates SOA and Microservices with REST, JMS (messaging), & multiple processes.

to begin: read the architecture & design document in the /docs directory.

there are 4 components of this application:

1. activemq
2. RESTAPIs
3. Services
4. UI

to demonstrate this application:

1. install and run a "vanilla" activemq on localhost
2. launch Services (see Services/README.txt)
3. launch RESTAPIs (see RESTAPIs/README.txt)
4. observe with the UI (see UI/README.txt)
