package tba.services;

import java.awt.geom.Point2D;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

import com.google.gson.Gson;

/**
 * MessageSender
 * 
 * <p>
 * publish location and direction information
 * </p>
 * https://examples.javacodegeeks.com/enterprise-java/jms/apache-activemq-hello-world-example/
 */
public class MessageSender {

  private static String messageBrokerUrl = ActiveMQConnection.DEFAULT_BROKER_URL;

  // all vehicles publish their status both a private and a public consolidating queue
  private static final String ALL_VEHICLES_STATUS = "ALL_VEHICLES_STATUS";
  
  // the vehicle for which i am a receiver
  private Vehicle vehicle;

  // used in the construction of a listen queue
  private Connection connection = null;
  private Session session = null;
  private MessageProducer privatePoducer = null;
  private MessageProducer allVehiclesProducer = null;
  
  public MessageSender(Vehicle vehicle) {
    this.vehicle = vehicle;
  }

  /**
   * cookie-cutter-code
   * @throws JMSException
   */
  public void setup() throws JMSException {
    // Get JMS connection from the server
    ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(messageBrokerUrl);
    connection = connectionFactory.createConnection();
    connection.start();
    // Create session for receiving messages
    session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
    // create the message queues
    Destination privateDestination = session.createQueue(vehicle.getPubId());
    Destination allVehiclesDestination = session.createQueue(ALL_VEHICLES_STATUS);
    // create the producers
    privatePoducer = session.createProducer(privateDestination);
    allVehiclesProducer = session.createProducer(allVehiclesDestination);
  }

  /**
   * release resources and clean things up
   * @throws JMSException
   */
  public void shutdown () throws JMSException {
    connection.close();
    connection = null;
    session = null;
    privatePoducer = null;
    allVehiclesProducer = null;
  }
  
  /**
   * publish vehicle stats.
   * 
   * @throws JMSException
   */
  public void publish() throws JMSException {
    // create the payload
    VehicleStatus vehicleStatus = new VehicleStatus(vehicle);
    String payload = new Gson().toJson(vehicleStatus);
    System.out.println("status = " + payload);
    // package the payload
    TextMessage message = session.createTextMessage(payload);
    // Send the message
    privatePoducer.send(message);
    allVehiclesProducer.send(message);
  }
  
  /**
   * VehicleStatus
   * <p>
   * POJO for pub & sub data. makes it easier to convert into various formats; e.g. json
   * </p>
   *
   */
  @SuppressWarnings("unused")
  private static class VehicleStatus {
    String pubId;
    String subId;
    Point2D location;
    String direction;
    
    VehicleStatus(Vehicle vehicle) {
      pubId = vehicle.getPubId();
      subId = vehicle.getSubId();
      location = vehicle.getLocation();
      direction = vehicle.getDirection();
    }
  }
  
}
