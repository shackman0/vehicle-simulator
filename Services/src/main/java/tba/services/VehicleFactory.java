package tba.services;


import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

/**
 * VehicleFactory
 * <p>
 * there's only 1 interface, and that is to create a vehicle.
 * </p>
 */
@WebServlet(urlPatterns = {"/create-vehicle"}, loadOnStartup = 1)
public class VehicleFactory extends HttpServlet {

  private static final long serialVersionUID = -7455316872823309027L;

  /**
   * this method implements the create vehicle api
   * note that vehicles, while created, are inactive until they told a direction in which to move.
   * @return pub and sub ids
   */
  @Override 
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
    // create a vehicle service
    Vehicle vehicle = new Vehicle();
    // construct result
    CreateResult createResult = new CreateResult();
    createResult.pubId = vehicle.getPubId();
    createResult.subId = vehicle.getSubId();
    String json = new Gson().toJson(createResult);
    // return result
    response.getOutputStream().print(json);
  }
  
  /**
   * CreateResult
   * <p>
   * POJO for pub & sub data. makes it easier to convert into various formats; e.g. json
   * </p>
   */
  @SuppressWarnings("unused")
  private class CreateResult {
    String pubId;
    String subId;
  }

}