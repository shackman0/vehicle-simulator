package tba.services;

import java.awt.geom.Point2D;
import java.util.UUID;

import javax.jms.JMSException;


/**
 * Vehicle
 * <p>
 * vehicles are simple minded. they travel in one direction until told to change directions.  
 * </p>
 */
public class Vehicle implements Runnable {
  
  /**
   * vehicles move at MOVEMENT_DISTANCE degrees every time they are told to move
   */  
  private static final double MOVEMENT_DISTANCE = 0.01d;
  
  /**
   * how long between location messages
   */
  private static final long SLEEP = 5000l;

  /**
   * keep moving until told to shut down
   */ 
  private boolean keepMoving;
  
  /**
   * Direction
   * <p>
   * direction specific commands
   * </p>
   */
  private enum Direction {
    NORTH {
      @Override
      public void move(Vehicle vehicle) {
        vehicle.location = createRoundedPoint(vehicle.location.getX(), vehicle.location.getY() + MOVEMENT_DISTANCE); 
      }
    },
    SOUTH {
      @Override
      public void move(Vehicle vehicle) {
        vehicle.location = createRoundedPoint(vehicle.location.getX(), vehicle.location.getY() - MOVEMENT_DISTANCE); 
      }
    },
    EAST {
      @Override
      public void move(Vehicle vehicle) {
        vehicle.location = createRoundedPoint(vehicle.location.getX() + MOVEMENT_DISTANCE, vehicle.location.getY()); 
      }
    },
    WEST {
      @Override
      public void move(Vehicle vehicle) {
        vehicle.location = createRoundedPoint(vehicle.location.getX() - MOVEMENT_DISTANCE, vehicle.location.getY()); 
      }
    };
    
    public abstract void move(Vehicle vehicle);
  }

  // keeps the double values readable at 2 decimal positions
  private static final double SCALE = 100d;
  
  // keeps the double values readable at 2 decimal positions
  static Point2D.Double createRoundedPoint (double x, double y) {
    double roundedX = Math.round(x * SCALE) / SCALE;
    double roundedY = Math.round(y * SCALE) / SCALE;
    Point2D.Double roundedPoint = new Point2D.Double(roundedX, roundedY);
    return roundedPoint;
  }
  
  private static final String PUB_ID_FORMAT = "pub_%s"; 
  private static final String SUB_ID_FORMAT = "sub_%s"; 
  
  /**
   * publish status here 
   */
  private String pubId;
  
  /**
   * receive direction commands here
   */
  private String subId;
  
  /**
   * x = lat
   * y = lon
   */
  private Point2D location = new Point2D.Double(0d, 0d);

  /**
   * current heading
   */
  private Direction direction = null;
  
  /**
   * since a newly created vehicle is stationary, we only instantiate the MessageSender when we are given a direction and start moving
   */ 
  private MessageSender messageSender = null;
  
  public Vehicle() {
    pubId = String.format(PUB_ID_FORMAT, UUID.randomUUID().toString()); 
    subId = String.format(SUB_ID_FORMAT, UUID.randomUUID().toString());
    
    MessageReceiver.launch(this);
  }

  public String getPubId() {
    return pubId;
  }

  public String getSubId() {
    return subId;
  }

  public String getDirection() {
    return direction.name();
  }
  
  public void setDirection(String direction) throws JMSException {
    this.direction = Direction.valueOf(direction.toUpperCase());
    startMoving();
  }
  
  public Point2D getLocation() {
    return location;
  }

  private void moveToNextPosition() {
    direction.move(this);
  }
  
  private void startMoving() throws JMSException {
    if (messageSender == null) {
      messageSender = new MessageSender(this);
      messageSender.setup();
      keepMoving = true;
      Thread thread = new Thread(this);
      thread.start();
    }
  }
  
  private void move() throws JMSException {
    do {
      moveToNextPosition();  
      messageSender.publish();
      // take a nap
      try {
        Thread.sleep(SLEEP);
      } catch (InterruptedException ex) {
        // ignore
      }
    } while (keepMoving);
  }
  
  @Override
  public void run() {
    try {
      move();
      shutdown();
    } catch (JMSException e) {
      e.printStackTrace();
    }
  }

  /**
   * call this to shutdown the movement thread
   */
  public void stop() {
    keepMoving = false;
  }

  /**
   * clean up, release resources
   * @throws JMSException
   */
  private void shutdown () throws JMSException {
    messageSender.shutdown();
    messageSender = null;
  }

}
