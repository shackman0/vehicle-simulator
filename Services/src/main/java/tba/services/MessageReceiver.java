package tba.services;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

/**
 * MessageReceiver
 * <p>
 * </p>
 * based on: https://examples.javacodegeeks.com/enterprise-java/jms/apache-activemq-hello-world-example/
 */
public class MessageReceiver implements Runnable {

  // the url to the activemq message broker
  private static String messageBrokerUrl = ActiveMQConnection.DEFAULT_BROKER_URL;
  
  /**
   * primary entry point for creating/activating a new vehicle
   * handles the creation and threading of the new vehicle
   * @param vehicle
   * @return
   */
  public static MessageReceiver launch(Vehicle vehicle) {
    MessageReceiver messageReceiver = new MessageReceiver(vehicle);
    Thread thread = new Thread(messageReceiver);
    thread.start();
    return messageReceiver;
  }
  
  // the vehicle for which i am a receiver
  private Vehicle vehicle;

  // used in the construction of a listen queue
  private Connection connection = null;
  private MessageConsumer consumer = null;

  // keep listening until told to shut down
  private boolean keepRunning = true;

  public MessageReceiver(Vehicle vehicle) {
    this.vehicle = vehicle;
  }

  @Override
  public void run() {
    try {
      setup();
      listen();
      shutdown();
    } catch (JMSException e) {
      e.printStackTrace();
    }
  }

  /**
   * call this to quiesce the thread
   */
  public void stop() {
    keepRunning = false;
  }
  
  /**
   * pretty much cookie cutter code here
   * @throws JMSException
   */
  private void setup() throws JMSException {
    // Get JMS connection from the server
    ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(messageBrokerUrl);
    connection = connectionFactory.createConnection();
    connection.start();

    // Create session for receiving messages
    Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

    // Get the message queue
    Destination destination = session.createQueue(vehicle.getSubId());

    // MessageConsumer is used for receiving (consuming) messages
    consumer = session.createConsumer(destination);
  }

  /**
   * release resources & clean things up
   * @throws JMSException
   */
  private void shutdown () throws JMSException {
    connection.close();
    connection = null;
    consumer = null;
  }
  
  // we stop listening every TIMEOUT milliseconds to check the keepRunning flag
  private static final long TIMEOUT = 5000L;
  
  /**
   * when we receive a message, update the vehicle's direction.
   * @throws JMSException
   */
  private void listen() throws JMSException {
    do {
      Message payload = consumer.receive(TIMEOUT);
      if (payload instanceof TextMessage) {
        TextMessage textMessage = (TextMessage)payload;
        String direction = textMessage.getText();
        System.out.println("direction = " + direction);
        vehicle.setDirection(direction);
      }
    } while (keepRunning);
  }

}
