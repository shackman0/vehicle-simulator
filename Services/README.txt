these steps are performed in a J2EE flavor of eclipse
 
build: 
in eclipse
1. right click on the project and select run as -> maven install

launch:
to launch the Services server
1. open a command prompt and navigate to ./tba/Services
2. run: mvn jetty:run -Djetty.http.port=8090

you can monitor the command window's output for Services activity
you may also want to browse to the activemq console http://localhost:8161/admin/ (admin,admin)